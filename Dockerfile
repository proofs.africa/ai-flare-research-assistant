###############################################
# Base Image
###############################################
FROM python:3.11.8-slim as python-base

ARG TSO_SETUP_URL

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
ENV PIP_NO_CACHE_DIR=off
ENV PIP_DISABLE_PIP_VERSION_CHECK=on
ENV PIP_DEFAULT_TIMEOUT=100
ENV POETRY_VERSION=1.0.5
ENV POETRY_HOME="/opt/poetry"
ENV POETRY_VIRTUALENVS_IN_PROJECT=true
ENV POETRY_NO_INTERACTION=1
ENV PYSETUP_PATH="/opt/pysetup"
ENV VENV_PATH="/opt/pysetup/.venv"

# prepend poetry and venv to path
ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

ENV TSO_SETUP_URL=${TSO_SETUP_URL}

###############################################
# Builder Image
###############################################
FROM python-base as builder-base
RUN apt-get update && apt-get install --no-install-recommends -y curl build-essential

# copy project requirement files here to ensure they will be cachedd.
WORKDIR $PYSETUP_PATH
COPY poetry.lock pyproject.toml ./

RUN pip install poetry==1.6.0

# install runtime deps - uses $POETRY_VIRTUALENVS_IN_PROJECT internally
RUN poetry install

# Install packages needed in runtime
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y sudo libusb-1.0 tzdata libffi-dev libssl-dev && \
    rm -rf /var/lib/apt/lists/*

###############################################
# Production Image
###############################################
FROM python-base as production
COPY --from=builder-base $PYSETUP_PATH $PYSETUP_PATH
COPY ai_flare_research_assistant /ai_flare_research_assistant/
COPY app.py app.py

ENTRYPOINT ["streamlit", "run", "app.py", "--server.fileWatcherType", "none", "--server.port=8501", "--server.address=0.0.0.0"]
