# Pinecone is a cloud-based Vector Database we'll use
# to store embeddings
from pinecone import Pinecone, ServerlessSpec

from ai_flare_research_assistant import PINECONE_KEY, PINECONE_INDEX_NAME

pc = Pinecone(
    api_key=PINECONE_KEY
)

# Now do stuff
if PINECONE_INDEX_NAME not in pc.list_indexes().names():
    pc.create_index(
        name=PINECONE_INDEX_NAME,
        dimension=1536,
        metric='euclidean',
        spec=ServerlessSpec(
            cloud='aws',
            region='us-west-2'
        )
    )

print(pc.describe_index(PINECONE_INDEX_NAME))
pinecone_index = pc.Index(PINECONE_INDEX_NAME)
