import os

from langchain_openai import OpenAIEmbeddings

from ai_flare_research_assistant import EMBEDDING_MODEL, OPENAI_KEY

# This references the text-embedding-ada-002 OpenAI model we'll use to create embeddings
# Both for indexing ground knowledge content, and later when searching ground knowledge
# For RAG documents to include in LLM Prompts

embed = OpenAIEmbeddings(model=EMBEDDING_MODEL, openai_api_key=OPENAI_KEY)

