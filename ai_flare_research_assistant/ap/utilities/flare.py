"""Util that calls GitHub."""
from __future__ import annotations

import json
from typing import TYPE_CHECKING, Any, Dict, List, Optional

import requests
from langchain_core.pydantic_v1 import BaseModel, Extra, root_validator
from langchain_core.utils import get_from_dict_or_env

from ai_flare_research_assistant.connectors.web.wallet_api import WalletApi


def _import_tiktoken() -> Any:
    """Import tiktoken."""
    try:
        import tiktoken
    except ImportError:
        raise ImportError(
            "tiktoken is not installed. "
            "Please install it with `pip install tiktoken`"
        )
    return tiktoken


class FlareAPIWrapper(BaseModel):
    """Wrapper for Flare API."""

    # github: Any  #: :meta private:
    wallet_instance: WalletApi  #: :meta private:

    # github_repository: Optional[str] = None
    # github_app_id: Optional[str] = None
    # github_app_private_key: Optional[str] = None
    # active_branch: Optional[str] = None
    # github_base_branch: Optional[str] = None

    class Config:
        """Configuration for this pydantic object."""
        arbitrary_types_allowed = True
        extra = Extra.forbid

    def get_block_number(self) -> Dict[str, Any]:
        """
        Fetches the current block number for the in context chain.
        Parameters:
        Returns:
            dict: A dictionary containing the in context chain block number,
        """

        return {
            "block_number": self.wallet_instance.get_current_block_number,
        }

    def get_reward_epoch(self) -> Dict[str, Any]:
        """
        Fetches the current reward epoch for the in context chain.
        Parameters:
        Returns:
            dict: A dictionary containing the in context chain current reward epoch,
        """

        return {
            "reward_epoch": self.wallet_instance.get_current_reward_epoch,
        }

    def get_price_epoch(self) -> Dict[str, Any]:
        """
        Fetches the current price epoch for the in context chain.
        Parameters:
        Returns:
            dict: A dictionary containing the in context chain current price epoch,
        """

        return self.wallet_instance.get_current_price_epoch

    def get_asset_price(self, ticker: str) -> Dict[str, Any]:
        """
        Fetches an asset price for the in context chain.
        Parameters:
            ticker: denoting the ticker symbol to be queried.
        Returns:
            dict: A dictionary containing the in context chain price of an asset,
        """

        return {
            "price": self.wallet_instance.get_price_by_asset(asset=ticker),
        }

    def run(self, mode: str, query: str) -> str:
        if mode == "get_block_number":
            return json.dumps(self.get_block_number())
        elif mode == "get_reward_epoch":
            return json.dumps(self.get_reward_epoch())
        elif mode == "get_price_epoch":
            return json.dumps(self.get_price_epoch())
        elif mode == "get_asset_price":
            return json.dumps(self.get_asset_price(str(query).upper()))
