from typing import Optional

from langchain_community.vectorstores.pinecone import Pinecone
from langchain_core.runnables import Runnable
from langchain_core.vectorstores import VectorStoreRetriever
from langchain.chains.combine_documents import create_stuff_documents_chain
from langchain.chains import create_retrieval_chain
from langchain import hub
from langchain_openai import ChatOpenAI

from ai_flare_research_assistant import OPENAI_KEY, GENAI_MODEL
from ai_flare_research_assistant.stores import embed
from ai_flare_research_assistant.stores.pinecone import pinecone_index


class FlareGeneralInfoChain:
    def __init__(self):
        self.chain: Optional[Runnable] = None

    def create(self):
        retriever = VectorStoreRetriever(
            vectorstore=Pinecone(index=pinecone_index, embedding=embed, text_key='context'))

        retrieval_qa_chat_prompt = hub.pull("langchain-ai/retrieval-qa-chat")
        llm = ChatOpenAI(openai_api_key=OPENAI_KEY, model_name=GENAI_MODEL, temperature=0.0)
        # retriever = ...
        combine_docs_chain = create_stuff_documents_chain(
            llm, retrieval_qa_chat_prompt
        )
        self.chain = create_retrieval_chain(retriever, combine_docs_chain)

    def run(self, query: str):
        return self.chain.invoke({
            "input": query
        })['answer']
