"""GitHub Toolkit."""
from typing import Dict, List

from langchain_core.pydantic_v1 import BaseModel, Field

from langchain_community.agent_toolkits.base import BaseToolkit
from langchain_community.tools import BaseTool
from ai_flare_research_assistant.ap.tools.flare.prompt import (
    GET_BLOCK_NUMBER_PROMPT,
    GET_REWARD_EPOCH_PROMPT,
    GET_PRICE_EPOCH_PROMPT,
    GET_ASSET_PRICE_PROMPT
)

from ai_flare_research_assistant.ap.tools.flare.tool import FlareAction
from ai_flare_research_assistant.ap.utilities.flare import FlareAPIWrapper


class NoInput(BaseModel):
    """Schema for operations that do not require any input."""

    no_input: str = Field("", description="No input required, e.g. `` (empty string).")


class Ticker(BaseModel):
    """Schema for operations that require a ticker symbol as input."""

    input: str = Field(
        ..., description="The ticker for the asset or price pair, e.g. `BTC`, `XRP`."
    )


class FlareToolkit(BaseToolkit):
    """Flare Toolkit.

    """

    tools: List[BaseTool] = []

    @classmethod
    def from_flare_api_wrapper(
            cls, flare_api_wrapper: FlareAPIWrapper
    ) -> "FlareToolkit":
        operations: List[Dict] = [
            {
                "mode": "get_block_number",
                "name": "get_block_number",
                "description": GET_BLOCK_NUMBER_PROMPT,
                "args_schema": NoInput,
            },
            {
                "mode": "get_reward_epoch",
                "name": "get_reward_epoch",
                "description": GET_REWARD_EPOCH_PROMPT,
                "args_schema": NoInput,
            },
            {
                "mode": "get_price_epoch",
                "name": "get_price_epoch",
                "description": GET_PRICE_EPOCH_PROMPT,
                "args_schema": NoInput,
            },
            {
                "mode": "get_asset_price",
                "name": "get_asset_price",
                "description": GET_ASSET_PRICE_PROMPT,
                "args_schema": Ticker,
            },

        ]
        tools = [
            FlareAction(
                name=action["name"],
                description=action["description"],
                mode=action["mode"],
                api_wrapper=flare_api_wrapper,
                args_schema=action.get("args_schema", None),
            )
            for action in operations
        ]
        return cls(tools=tools)

    def get_tools(self) -> List[BaseTool]:
        """Get the tools in the toolkit."""
        return self.tools
