from typing import Optional, Type

from langchain_core.tools import BaseTool
from pydantic import BaseModel, Field

from ai_flare_research_assistant.ap.tools import get_asset_price


class AssetPriceCheckInput(BaseModel):
    """Input for Stock price check."""

    ticker: str = Field(..., description="Ticker for the asset price pair. ")


class AssetPriceTool(BaseTool):
    name = "get_current_price_for_asset"
    description = ("Useful for when you need to find out the current on-chain price for a price pair, ftso or asset. "
                   "You should input the relevant ticker for the supported assets as provided in the prompts. ")

    args_schema: Optional[Type[BaseModel]] = AssetPriceCheckInput

    def _run(self, ticker: str):
        # print("i'm running")
        # price_response = get_asset_price(ticker)

        return 1000

    def _arun(self, ticker: str):
        raise NotImplementedError("This tool does not support async")
