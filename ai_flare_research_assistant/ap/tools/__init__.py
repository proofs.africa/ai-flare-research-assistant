from typing import Any

from ai_flare_research_assistant.connectors.web.wallet_api import WalletApi

URL = 'https://flare-api.flare.network/ext/C/rpc'
CHAIN = 'flare'
SUBMITTER = '0x1000000000000000000000000000000000000003'

wallet: WalletApi = WalletApi(jsonrpc_url=URL, chain=CHAIN, price_submitter_address=SUBMITTER)


def get_current_blockchain_info():
    pass


def get_asset_price(tso: str):
    return wallet.get_price_by_asset(asset=tso)
