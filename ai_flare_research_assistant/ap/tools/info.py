from typing import Optional, Type

from langchain_core.tools import BaseTool
from pydantic import BaseModel, Field


class BlockchainNameInput(BaseModel):
    """Input for Stock price check."""

    blockchain: str = Field(..., description="Ticker symbol for stock or index")


class CurrentBlockchainInfoTool(BaseTool):
    name = "get_current_blockchain_info"
    description = ("Useful for when you need to find out the price of stock. You should input the stock ticker used on "
                   "the yfinance API")

    args_schema: Optional[Type[BaseModel]] = BlockchainNameInput

    def _run(self, blockchain: str):
        # print("i'm running")
        price_response = get_stock_price(blockchain)

        return price_response

    def _arun(self, blockchain: str):
        raise NotImplementedError("This tool does not support async")
