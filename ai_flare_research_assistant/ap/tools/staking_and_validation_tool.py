from typing import Optional, Type

from langchain_core.tools import BaseTool
from pydantic import BaseModel, Field

from ai_flare_research_assistant.ap.agents.flare_staking_and_validation_agent import FlareStakingAndValidationAgent
from ai_flare_research_assistant.ap.chains.flare_general_info_chain import FlareGeneralInfoChain


class UserQueryInfo(BaseModel):
    """Input for Stock price check."""

    query: str = Field(..., description="The full query from the user. ")


class StakingAndValidationTool(BaseTool):
    name = "get_flare_staking_and_validation_data"
    description = ("Useful for when you need to provide on chain data about staking and validation on the Flare chain. "
                   "The requested data may include validators, delegators, staked amounts, self bonded amounts "
                   "and any data that may have to do with the present state of the Flare blockchain. "
                   "This may also include analysis on the above mentioned context. ")

    args_schema: Optional[Type[BaseModel]] = UserQueryInfo

    def _run(self, query: str):
        agent = FlareStakingAndValidationAgent().create()
        answer = agent.run(query)
        # print(f"ANNSSWEEER {answer}")
        return answer

    def _arun(self, query: str):
        raise NotImplementedError("This tool does not support async")
