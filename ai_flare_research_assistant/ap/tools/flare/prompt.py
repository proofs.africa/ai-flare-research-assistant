# flake8: noqa
GET_BLOCK_NUMBER_PROMPT = """This tool used in fetching the in context blockchain block number. Either 'flare' or 
                        'songbird' are the two input choices. If neither are given, 'flare' is to be used as input."""

GET_REWARD_EPOCH_PROMPT = """This tool used in fetching the in context blockchain reward epoch. Either 'flare' or 
                        'songbird' are the two input choices. If neither are given, 'flare' is to be used as input."""

GET_PRICE_EPOCH_PROMPT = """This tool used in fetching the in context blockchain price epoch. Either 'flare' or 
                        'songbird' are the two input choices. If neither are given, 'flare' is to be used as input."""

GET_ASSET_PRICE_PROMPT = """Useful for when you need to find out the current on-chain price for a price pair, or asset. 
                        **VERY IMPORTANT**: You must specify the ticker as an a string. The ticker must be 
                        formatted uppercase."""