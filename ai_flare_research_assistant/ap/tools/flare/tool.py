"""
This tool allows agents to interact with the Flare or Songbird blockchain
web wallets.

"""
from typing import Optional, Type

from langchain_core.callbacks import CallbackManagerForToolRun
from langchain_core.pydantic_v1 import BaseModel, Field
from langchain_core.tools import BaseTool

from ai_flare_research_assistant.ap.utilities.flare import FlareAPIWrapper


class FlareAction(BaseTool):
    """Tool for interacting with the Flare or Songbird web3 wallet."""

    api_wrapper: FlareAPIWrapper = Field(default_factory=FlareAPIWrapper)
    mode: str
    name: str = ""
    description: str = ""
    args_schema: Optional[Type[BaseModel]] = None

    def _run(
        self,
        input: Optional[str] = "",
        run_manager: Optional[CallbackManagerForToolRun] = None,
    ) -> str:
        """Use the GitHub API to run an operation."""
        if not input or input == "{}":
            # Catch other forms of empty input that GPT-4 likes to send.
            input = ""
        return self.api_wrapper.run(self.mode, input)
