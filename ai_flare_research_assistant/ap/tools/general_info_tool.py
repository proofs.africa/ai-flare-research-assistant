from typing import Optional, Type

from langchain_core.tools import BaseTool
from pydantic import BaseModel, Field

from ai_flare_research_assistant.ap.chains.flare_general_info_chain import FlareGeneralInfoChain


class UserQueryInfo(BaseModel):
    """Input for Stock price check."""

    query: str = Field(..., description="The full query from the user. ")


class GeneralInfoTool(BaseTool):
    name = "get_flare_general_info"
    description = ("Useful for when you need to find out general knowledge and educational info about the flare "
                   "ecosystem. Takes a user query as an argument. ")

    args_schema: Optional[Type[BaseModel]] = UserQueryInfo

    def _run(self, query: str):
        # print("i'm running")
        # price_response = get_stock_price(blockchain)
        fgic = FlareGeneralInfoChain()
        fgic.create()

        return fgic.run(query=query)

    def _arun(self, query: str):
        raise NotImplementedError("This tool does not support async")
