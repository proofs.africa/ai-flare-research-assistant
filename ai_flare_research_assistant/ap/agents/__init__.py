import numpy as np
import pendulum
from langchain.agents import create_openai_tools_agent, AgentExecutor
from langchain_core.prompts import ChatPromptTemplate, MessagesPlaceholder
from langchain_openai import ChatOpenAI

from ai_flare_research_assistant.connectors.rpc.flarepy.api.platform import get_current_validators


def create_agent(llm: ChatOpenAI, tools: list, system_prompt: str):
    # Each worker node will be given a name and some tools.
    prompt = ChatPromptTemplate.from_messages(
        [
            (
                "system",
                system_prompt,
            ),
            # MessagesPlaceholder(variable_name="messages"),
            MessagesPlaceholder(variable_name="agent_scratchpad"),
        ]
    )
    agent = create_openai_tools_agent(llm, tools, prompt)
    executor = AgentExecutor(agent=agent, tools=tools)
    return executor


def get_total_delegated_to_validator(delegators: list):
    # print(delegators[0])
    total = np.sum([int(delegator['stakeAmount']) / 1000000000 for delegator in delegators if delegator['stakeAmount'] is not None])
    return total


def load_validators_data():
    current_validators = get_current_validators()['validators']
    validators: list = []
    delegators: list = []
    for validator in current_validators:
        if validator['delegators'] is None:
            continue

        delegated = get_total_delegated_to_validator(validator['delegators'])
        validators.append({
            "node_id": validator['nodeID'],
            "validator": str(validator['nodeID'])[0:12],
            "start": validator['startTime'],
            "end": validator['endTime'],
            "self_bonded_amount": int(validator['stakeAmount']) / 1000000000,
            "delegated_amount": delegated,
            "total_staked_amount": delegated + (int(validator['stakeAmount']) / 1000000000),
            "connected": validator['connected'],
            "tx": validator['txID'],
        })

        for delegator in validator['delegators']:
            s = int(delegator['startTime'])
            e = int(delegator['endTime'])
            # days = pendulum.from_timestamp(s).diff(pendulum.from_timestamp(e)).days
            delegators.append({
                "validator": str(delegator['nodeID'])[0:12],
                "start": pendulum.from_timestamp(s).to_iso8601_string(),
                "end": pendulum.from_timestamp(e).to_iso8601_string(),
                "days": pendulum.from_timestamp(s).diff(pendulum.from_timestamp(e)).days,
                "delegated_amount": int(delegator['stakeAmount']) / 1000000000,
                "tx": delegator['txID']
            })

    return validators, delegators
