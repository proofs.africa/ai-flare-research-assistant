from typing import Optional

from langchain import hub
from langchain.agents import AgentExecutor, create_structured_chat_agent
from langchain_openai import ChatOpenAI

from ai_flare_research_assistant.ap.agents import create_agent
from ai_flare_research_assistant.ap.toolkits.flare.toolkit import FlareToolkit
from ai_flare_research_assistant.ap.tools.general_info_tool import GeneralInfoTool
from ai_flare_research_assistant.ap.tools.staking_and_validation_tool import StakingAndValidationTool
from ai_flare_research_assistant.ap.utilities.flare import FlareAPIWrapper
from ai_flare_research_assistant.connectors.web.wallet_api import WalletApi

URL = 'https://flare-api.flare.network/ext/C/rpc'
CHAIN = 'flare'
SUBMITTER = '0x1000000000000000000000000000000000000003'


class FlareBlockchainDataAgent:
    def __init__(self):
        # Get the prompt to use - you can modify this!
        self.prompt = hub.pull("hwchase17/structured-chat-agent")
        # print(self.prompt.messages)
        # self.prompt = """
        #         Useful for when there is a request for the present state data on the Flare or Songbird blockchain.
        #
        #         Examples of questions that can be answered are:-
        #         - Current blockchain block number
        #         - Current blockchain reward epoch
        #         - Current blockchain price epoch
        #         - Current on-chain price for a price pair, or asset.
        #
        #     """
        self.agent_executor: Optional[AgentExecutor] = None

    def create(self) -> AgentExecutor:
        wallet = WalletApi(jsonrpc_url=URL, chain=CHAIN, price_submitter_address=SUBMITTER)
        flare = FlareAPIWrapper(wallet_instance=wallet)
        toolkit = FlareToolkit.from_flare_api_wrapper(flare)
        tools: list = toolkit.get_tools()
        print("Available tools:")
        for tool in tools:
            print("\t" + tool.name)
        # self.agent_executor = create_agent(llm=ChatOpenAI(temperature=0, model="gpt-3.5-turbo-0613"),
        #                                    tools=tools,
        #                                    system_prompt=self.prompt
        #                                    )
        #
        # return self.agent_executor

        llm = ChatOpenAI(model="gpt-3.5-turbo-0613", temperature=0)
        agent = create_structured_chat_agent(llm, tools, self.prompt)

        self.agent_executor = AgentExecutor(agent=agent, tools=tools, verbose=True)
        self.agent_executor.handle_parsing_errors = True

        return self.agent_executor
