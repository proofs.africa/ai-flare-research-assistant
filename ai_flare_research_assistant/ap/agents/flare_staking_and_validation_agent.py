from typing import Optional

import pandas as pd
from langchain.agents import AgentExecutor, AgentType
from langchain_core.runnables import Runnable
from langchain_experimental.agents.agent_toolkits import create_pandas_dataframe_agent
from langchain_openai import ChatOpenAI

from ai_flare_research_assistant.ap.agents import load_validators_data


class FlareStakingAndValidationAgent:
    def __init__(self):
        self.df: Optional[pd.DataFrame] = None
        self.agent_executor: Optional[AgentExecutor] = None

    def create(self) -> AgentExecutor:
        # source data
        validators, delegators = load_validators_data()
        # convert sourced to dataframe
        validators_df: pd.DataFrame = pd.DataFrame(validators)
        # print(validators_df)
        self.agent_executor = create_pandas_dataframe_agent(
            ChatOpenAI(temperature=0, model="gpt-3.5-turbo-0613"),
            validators_df,
            verbose=True,
            agent_type=AgentType.OPENAI_FUNCTIONS,
        )

        self.agent_executor.handle_parsing_errors = True

        return self.agent_executor


