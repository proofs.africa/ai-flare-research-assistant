from typing import Optional

from langchain.agents import AgentExecutor, create_openai_tools_agent
from langchain_core.prompts import ChatPromptTemplate, MessagesPlaceholder
from langchain_openai import ChatOpenAI

from ai_flare_research_assistant.ap.agents import create_agent
from ai_flare_research_assistant.ap.tools.general_info_tool import GeneralInfoTool


class FlareGeneralInformationAgent:
    def __init__(self):
        self.agent_executor: Optional[AgentExecutor] = None
        self.prompt: str = \
            """Useful for when you need to find out general knowledge and educational info about the flare. """

    def create(self) -> AgentExecutor:
        self.agent_executor = create_agent(llm=ChatOpenAI(temperature=0, model="gpt-3.5-turbo-0613"),
                                           tools=[GeneralInfoTool()],
                                           system_prompt=self.prompt
                                           )

        return self.agent_executor
