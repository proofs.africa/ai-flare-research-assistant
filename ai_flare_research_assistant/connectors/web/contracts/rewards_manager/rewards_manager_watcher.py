import asyncio
from typing import List

from web3 import Web3
from web3.datastructures import AttributeDict

from ai_flare_research_assistant import app_logger
from ai_flare_research_assistant.chain.oracle.contracts.tso_contract_base import TsoContractBase
from ai_flare_research_assistant.chain.watcher.base_watcher import BaseWatcher
from ai_flare_research_assistant.chain.watcher.contract_event_logs import ContractEventLogger
from ai_flare_research_assistant.chain.watcher.websocket_watcher import WSNewBlocksWatcher
from ai_flare_research_assistant.core.utils.async_utils import safe_ensure_future

REWARDS_DISTRIBUTED_EVENT_NAME = "RewardsDistributed"
REWARD_CLAIMED_EVENT_NAME = "RewardClaimed"


class RewardsManagerWatcher(BaseWatcher):

    def __init__(self,
                 w3: Web3,
                 rewards_manager: TsoContractBase,
                 blocks_watcher: WSNewBlocksWatcher,
                 new_blocks_queue: asyncio.Queue,
                 output: asyncio.Queue):
        super().__init__(w3)
        self._blocks_watcher: WSNewBlocksWatcher = blocks_watcher
        self._rewards_manager = rewards_manager
        self._output = output
        self._rewards_manager_contract = rewards_manager.contract
        self._contract_event_logger = ContractEventLogger(w3, rewards_manager.address, rewards_manager.abi)
        self._poll_rewards_manager_logs_task: asyncio.Task = None
        self._new_blocks_queue: asyncio.Queue = new_blocks_queue

    async def start_network(self):
        if self._poll_rewards_manager_logs_task is not None:
            await self.stop_network()
        self._poll_rewards_manager_logs_task = safe_ensure_future(self.poll_rewards_manager_logs_loop())

    async def stop_network(self):
        if self._poll_rewards_manager_logs_task is not None:
            self._poll_rewards_manager_logs_task.cancel()
            self._poll_rewards_manager_logs_task = None

    async def poll_rewards_manager_logs_loop(self):
        while True:
            try:
                new_blocks: List[AttributeDict] = await self._new_blocks_queue.get()

                rewards_distributed_entries = await self._contract_event_logger.get_new_entries_from_logs(
                    REWARDS_DISTRIBUTED_EVENT_NAME,
                    new_blocks
                )

                governance_updated_entries = await self._contract_event_logger.get_new_entries_from_logs(
                    REWARD_CLAIMED_EVENT_NAME,
                    new_blocks
                )
                for entry in rewards_distributed_entries:
                    await self._handle_event_data(entry)
                for entry in governance_updated_entries:
                    await self._handle_event_data(entry)

            except asyncio.CancelledError:
                raise
            except asyncio.TimeoutError:
                await asyncio.sleep(0.5)
                continue
            except Exception:
                app_logger.error("Unknown error trying to fetch new events from TSO registry contract.",
                                 exc_info=True,
                                 app_warning_msg="Unknown error trying to fetch new events from TSO registry "
                                                 "contract. Check wallet network connection.")

    async def _handle_event_data(self, event_data: AttributeDict):
        event_type: str = event_data["event"]
        timestamp: float = float(await self._blocks_watcher.get_timestamp_for_block(event_data["blockHash"]))
        tx_hash: str = event_data["transactionHash"].hex()
        if event_type == REWARDS_DISTRIBUTED_EVENT_NAME:
            self.rewards_distributed_event(timestamp, tx_hash, event_data)
        elif event_type == REWARD_CLAIMED_EVENT_NAME:
            self.governance_updated_event(timestamp, tx_hash, event_data)
        else:
            app_logger.warning(f"Received log with unrecognized event type - '{event_type}'.")

    def rewards_distributed_event(self, timestamp: float, tx_hash: str, event_data: AttributeDict):
        event_args: AttributeDict = event_data["args"]

        proposed_governance: str = event_args["proposed_governance"]

