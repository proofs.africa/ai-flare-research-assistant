import asyncio
from typing import List

from web3 import Web3
from web3.datastructures import AttributeDict

from ai_flare_research_assistant import app_logger
from ai_flare_research_assistant.chain.oracle.contracts.tso.tso_contract import TSOContract
from ai_flare_research_assistant.chain.oracle.contracts.tso_contract_base import TsoContractBase
from ai_flare_research_assistant.chain.watcher.base_watcher import BaseWatcher
from ai_flare_research_assistant.chain.watcher.contract_event_logs import ContractEventLogger
from ai_flare_research_assistant.chain.watcher.websocket_watcher import WSNewBlocksWatcher
from ai_flare_research_assistant.core.utils.async_utils import safe_ensure_future
from ai_flare_research_assistant.shared_setup import SharedSetup

PRICE_HASHES_SUBMITTED_EVENT_NAME = "PriceHashesSubmitted"
PRICES_REVEALED_EVENT_NAME = "PricesRevealed"


class PriceSubmitterWatcher(BaseWatcher):

    def __init__(self,
                 w3: Web3,
                 price_submitter: TsoContractBase,
                 blocks_watcher: WSNewBlocksWatcher,
                 tsos: List[TSOContract],
                 new_blocks_queue: asyncio.Queue,
                 output: asyncio.Queue):
        super().__init__(w3)
        self._blocks_watcher: WSNewBlocksWatcher = blocks_watcher
        self._price_submitter = price_submitter
        self._tsos = {tso.address: tso.name for tso in tsos}
        self._output = output
        self._price_submitter_contract = price_submitter.contract
        self._contract_event_logger = ContractEventLogger(w3, price_submitter.address, price_submitter.abi)
        self._poll_price_submitter_logs_task: asyncio.Task = None
        self._new_blocks_queue: asyncio.Queue = new_blocks_queue

    async def start_network(self):
        if self._poll_price_submitter_logs_task is not None:
            await self.stop_network()
        self._poll_price_submitter_logs_task = safe_ensure_future(self.poll_price_submitter_logs_loop())

    async def stop_network(self):
        if self._poll_price_submitter_logs_task is not None:
            self._poll_price_submitter_logs_task.cancel()
            self._poll_price_submitter_logs_task = None

    async def poll_price_submitter_logs_loop(self):
        while True:
            try:
                new_blocks: List[AttributeDict] = await self._new_blocks_queue.get()

                # price_hashes_submitted_entries = await self._contract_event_logger.get_new_entries_from_logs(
                #     PRICE_HASHES_SUBMITTED_EVENT_NAME,
                #     new_blocks
                # )

                prices_revealed_entries = await self._contract_event_logger.get_new_entries_from_logs(
                    PRICES_REVEALED_EVENT_NAME,
                    new_blocks
                )

                # print(f"Entries {len(price_hashes_submitted_entries)} {len(prices_revealed_entries)}")

                # for deposit_entry in price_hashes_submitted_entries:
                #     await self._handle_event_data(deposit_entry)
                for withdrawal_entry in prices_revealed_entries:
                    await self._handle_event_data(withdrawal_entry)

            except asyncio.CancelledError:
                raise
            except asyncio.TimeoutError:
                continue
            except Exception:
                app_logger.error("Unknown error trying to fetch new events from WETH contract.", exc_info=True,
                                 app_warning_msg="Unknown error trying to fetch new events from WETH contract. "
                                                 "Check wallet network connection")

    async def _handle_event_data(self, event_data: AttributeDict):
        event_type: str = event_data["event"]
        timestamp: float = float(await self._blocks_watcher.get_timestamp_for_block(event_data["blockHash"]))
        tx_hash: str = event_data["transactionHash"].hex()
        if event_type == PRICE_HASHES_SUBMITTED_EVENT_NAME:
            self.handle_price_hashes_submitted_event(timestamp, tx_hash, event_data)
        elif event_type == PRICES_REVEALED_EVENT_NAME:
            self.handle_prices_revealed_event(timestamp, tx_hash, event_data)
        else:
            app_logger.warning(f"Received log with unrecognized event type - '{event_type}'.")

    def handle_price_hashes_submitted_event(self, timestamp: float, tx_hash: str, event_data: AttributeDict):
        event_args: AttributeDict = event_data["args"]

        # print(f"Event args {event_args}")
        epoch_id: int = event_args["epochId"]
        ftsos: list = event_args["ftsos"]
        hashes: list = event_args["hashes"]
        timestamp: float = event_args["timestamp"]
        submitter_address: str = event_args["submitter"]

        safe_ensure_future(self._output.put({"type": "price_hashes_submitted", "chain": SharedSetup.get_instance().chain["name"],
                                             "data": {"epoch": epoch_id,
                                                      "ftsos": ftsos,
                                                      "submitter_address": submitter_address,
                                                      "tso_map": self._tsos,
                                                      "timestamp": timestamp}}))

    def handle_prices_revealed_event(self, timestamp: float, tx_hash: str, event_data: AttributeDict):
        event_args: AttributeDict = event_data["args"]

        # print(f"Event args {event_args}")
        voter_address: str = event_args["voter"]
        epoch_id: int = event_args["epochId"]
        ftsos: list = event_args["ftsos"]
        prices: list = event_args["prices"]
        timestamp: float = event_args["timestamp"]

        safe_ensure_future(self._output.put({"type": "price_revealed", "chain": SharedSetup.get_instance().chain["name"],
                                             "data": {"epoch": epoch_id,
                                                      "ftsos": ftsos,
                                                      "prices": prices,
                                                      "voter": voter_address,
                                                      "tso_map": self._tsos,
                                                      "timestamp": timestamp}}))
