import json
import os

from web3 import Web3

from ai_flare_research_assistant.shared_setup import SharedSetup

from ai_flare_research_assistant.connectors.web.contracts.tso_contract_base import TsoContractBase


class TSOContract(TsoContractBase):

    def __init__(self,
                 w3: Web3,
                 address: str,
                 chain: str,
                 name: str):
        super().__init__(w3, address, chain)
        self._name = name

    @property
    def name(self):
        return self._name

    @property
    def abi(self):
        with open(
                os.path.join(os.path.dirname(__file__), f'tso_contract_abi.{self.chain}.json')) as contract_abi:
            data: dict = json.load(contract_abi)
        return data["abi"]
