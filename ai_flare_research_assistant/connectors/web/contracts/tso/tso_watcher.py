import asyncio
import logging
from typing import List, Dict

import cytoolz
from komolibs.core.utils.async_utils import safe_gather
from web3 import Web3
from web3.datastructures import AttributeDict

from ai_flare_research_assistant import app_logger
from ai_flare_research_assistant.chain.oracle.contracts.tso.tso_contract import TSOContract
from ai_flare_research_assistant.chain.watcher.base_watcher import BaseWatcher
from ai_flare_research_assistant.chain.watcher.contract_event_logs import ContractEventLogger
from ai_flare_research_assistant.chain.watcher.websocket_watcher import WSNewBlocksWatcher
from ai_flare_research_assistant.core.utils.async_utils import safe_ensure_future
from ai_flare_research_assistant.shared_setup import SharedSetup

LOW_TURN_EVENT_NAME = "LowTurnout"
PRICE_EPOCH_INITIALIZED_ON_FTSO_EVENT_NAME = "PriceEpochInitializedOnFtso"
PRICE_FINALIZED_EVENT_NAME = "PriceFinalized"
PRICE_HASH_SUBMITTED_EVENT_NAME = "PriceHashSubmitted"
PRICE_REVEALED_EVENT_NAME = "PriceRevealed"


class TsoWatcher(BaseWatcher):

    def __init__(self,
                 w3: Web3,
                 blocks_watcher: WSNewBlocksWatcher,
                 tsos: List[TSOContract],
                 new_blocks_queue: asyncio.Queue,
                 output: asyncio.Queue):
        super().__init__(w3)
        self._blocks_watcher: WSNewBlocksWatcher = blocks_watcher
        self._tsos: List[TSOContract] = tsos
        self._output = output
        self._contract_event_loggers: Dict[str, ContractEventLogger] = {}
        self._new_blocks_queue: asyncio.Queue = new_blocks_queue
        self._poll_tso_logs_task: asyncio.Task = None

    async def start_network(self):
        for tso in self._tsos:
            self._contract_event_loggers[tso.address] = ContractEventLogger(self._w3, tso.address, tso.abi)

        if self._poll_tso_logs_task is not None:
            await self.stop_network()
        self._poll_tso_logs_task = safe_ensure_future(self.poll_tso_logs_loop())

    async def stop_network(self):
        if self._poll_tso_logs_task is not None:
            self._poll_tso_logs_task.cancel()
            self._poll_tso_logs_task = None

    async def poll_tso_logs_loop(self):
        while True:
            try:
                new_blocks: List[AttributeDict] = await self._new_blocks_queue.get()
                low_turnout_tasks = []
                price_epoch_initialized_tasks = []
                price_finalized_tasks = []
                # print(f"TYPE OF STRUCT {type(new_blocks)}")
                for tso in self._tsos:
                    contract_event_logger: ContractEventLogger = self._contract_event_loggers[tso.address]
                    low_turnout_tasks.append(
                        contract_event_logger.get_new_entries_from_logs(LOW_TURN_EVENT_NAME,
                                                                        new_blocks)
                    )
                    price_epoch_initialized_tasks.append(
                        contract_event_logger.get_new_entries_from_logs(PRICE_EPOCH_INITIALIZED_ON_FTSO_EVENT_NAME,
                                                                        new_blocks)
                    )
                    price_finalized_tasks.append(
                        contract_event_logger.get_new_entries_from_logs(PRICE_FINALIZED_EVENT_NAME,
                                                                        new_blocks)
                    )

                raw_low_turnout_entries = await safe_gather(*low_turnout_tasks)
                raw_price_epoch_initialized_entries = await safe_gather(*price_epoch_initialized_tasks)
                raw_price_finalized_entries = await safe_gather(*price_finalized_tasks)
                low_turnout_entries = list(cytoolz.concat(raw_low_turnout_entries))
                price_epoch_initialized_entries = list(cytoolz.concat(raw_price_epoch_initialized_entries))
                price_finalized_entries = list(cytoolz.concat(raw_price_finalized_entries))

                for entry in low_turnout_entries:
                    await self._handle_event_data(entry)
                for entry in price_epoch_initialized_entries:
                    await self._handle_event_data(entry)
                for entry in price_finalized_entries:
                    await self._handle_event_data(entry)

            except asyncio.CancelledError:
                raise
            except asyncio.TimeoutError:
                continue
            except Exception:
                app_logger.error("Unknown error trying to fetch new events from TSO contract.",
                                 exc_info=True,
                                 app_warning_msg="Unknown error trying to fetch new events from TSO "
                                                 "contract. Check wallet network connection.")

    async def _handle_event_data(self, event_data: AttributeDict):
        event_type: str = event_data["event"]
        timestamp: float = float(await self._blocks_watcher.get_timestamp_for_block(event_data["blockHash"]))
        tx_hash: str = event_data["transactionHash"].hex()
        if event_type == LOW_TURN_EVENT_NAME:
            self.low_turnout_event(timestamp, tx_hash, event_data)
        elif event_type == PRICE_EPOCH_INITIALIZED_ON_FTSO_EVENT_NAME:
            self.price_epoch_initialized_on_ftso_event(timestamp, tx_hash, event_data)
        elif event_type == PRICE_FINALIZED_EVENT_NAME:
            self.price_finalized_event(timestamp, tx_hash, event_data)
        else:
            app_logger.warning(f"Received log with unrecognized event type - '{event_type}'.")

    def low_turnout_event(self, timestamp: float, tx_hash: str, event_data: AttributeDict):
        event_args: AttributeDict = event_data["args"]
        app_logger.debug(f"Low Turnout Event args {event_args}")

        epoch_id: int = event_args["epochId"]
        nat_turnout: int = event_args["natTurnout"]
        low_nat_turnout_threshold_bips: int = event_args["lowNatTurnoutThresholdBIPS"]
        timestamp: int = event_args["timestamp"]

        safe_ensure_future(self._output.put({"type": "low_turnout",
                                             "chain": SharedSetup.get_instance().chain["name"],
                                             "data": {"epoch": epoch_id,
                                                      "nat_turnout": nat_turnout,
                                                      "low_nat_turnout_threshold_bips": low_nat_turnout_threshold_bips,
                                                      "timestamp": timestamp}}))

    def price_epoch_initialized_on_ftso_event(self, timestamp: float, tx_hash: str, event_data: AttributeDict):
        event_args: AttributeDict = event_data["args"]
        self.log_with_clock(log_level=logging.DEBUG, msg=f"Epoch Initialized Event - {event_args}")

        epoch_id: int = event_args["epochId"]
        end_time: int = event_args["endTime"]
        timestamp: int = event_args["timestamp"]

        self._output.put_nowait({"type": "price_epoch_initialized",
                                 "chain": SharedSetup.get_instance().chain["name"],
                                 "data": {"epoch": epoch_id,
                                          "end_time": end_time,
                                          "timestamp": timestamp}})

    def price_finalized_event(self, timestamp: float, tx_hash: str, event_data: AttributeDict):
        event_args: AttributeDict = event_data["args"]
        # app_logger.info(f"Price Finalized Event - {event_data}")

        epoch_id: int = event_args["epochId"]
        price: int = event_args["price"]
        rewarded_ftso: int = event_args["rewardedFtso"]
        low_elastic_reward_price: int = event_args["lowElasticBandRewardPrice"]
        low_reward_price: int = event_args["lowIQRRewardPrice"]
        high_reward_price: int = event_args["highIQRRewardPrice"]
        high_elastic_reward_price: int = event_args["highElasticBandRewardPrice"]
        finalization_type: int = event_args["finalizationType"]
        timestamp: int = event_args["timestamp"]

        for _tso in self._tsos:
            # self.logger().info(f"Event data Address is  {event_data['address']}")
            if _tso.address == event_data["address"]:
                # app_logger.info(f"Found {_tso.name} Address  {event_data['address']}")

                safe_ensure_future(self._output.put({"type": "price_finalized",
                                                     "chain": SharedSetup.get_instance().chain["name"],
                                                     "data": {"epoch": epoch_id,
                                                              "address": event_data["address"],
                                                              "tso": _tso.name,
                                                              "price": price / 100000,
                                                              "rewarded_ftso": rewarded_ftso,
                                                              "low_elastic_reward_price": low_elastic_reward_price / 100000,
                                                              "low_reward_price": low_reward_price / 100000,
                                                              "high_reward_price": high_reward_price / 100000,
                                                              "high_elastic_reward_price": high_elastic_reward_price / 100000,
                                                              "finalization_type": finalization_type,
                                                              "timestamp": timestamp}}))

    def price_hash_submitted_event(self, timestamp: float, tx_hash: str, event_data: AttributeDict):
        event_args: AttributeDict = event_data["args"]
        app_logger.debug(f"Price Submitted Event - {event_args}")

        submitter: str = event_args["submitter"]
        epoch_id: int = event_args["epochId"]
        hash: bytes = event_args["hash"]
        timestamp: int = event_args["timestamp"]

        safe_ensure_future(self._output.put({"type": "price_hash_submitted",
                                             "chain": SharedSetup.get_instance().chain["name"],
                                             "data": {"epoch": epoch_id,
                                                      "submitter": submitter,
                                                      "hash": hash,
                                                      "timestamp": timestamp}}))

