from typing import Union

from web3 import Web3
from web3.contract import Contract


class TsoContractBase:

    def __init__(self,
                 w3: Web3,
                 address: str,
                 chain: str):
        self._w3: Web3 = w3
        self._address: str = address
        self._chain: str = chain
        self._name: str = ""
        self._contract: Contract = self._w3.eth.contract(address=self._address, abi=self.abi)

    @property
    def name(self):
        return NotImplementedError

    @property
    def abi(self):
        return NotImplementedError

    @classmethod
    def get_name_from_contract(cls, contract: Contract) -> str:
        raw_name: Union[str, bytes] = contract.functions.name().call()
        if isinstance(raw_name, bytes):
            retval: str = raw_name.split(b"\x00")[0].decode("utf8")
        else:
            retval: str = raw_name
        return retval

    @property
    def chain(self) -> str:
        return self._chain

    @property
    def address(self) -> str:
        return self._address

    @property
    def contract(self) -> Contract:
        return self._contract
