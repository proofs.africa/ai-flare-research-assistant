import json
import os

from web3 import Web3

from ai_flare_research_assistant.connectors.web.contracts.tso_contract_base import TsoContractBase


class TSOVoterWhiteListerContract(TsoContractBase):

    def __init__(self,
                 w3: Web3,
                 address: str,
                 chain: str):
        super().__init__(w3, address, chain)

    @property
    def name(self):
        return "voter_white_lister"

    @property
    def abi(self):
        with open(
                os.path.join(os.path.dirname(__file__), f'voter_white_lister_contract_abi.{self.chain}.json')) as contract_abi:
            data: dict = json.load(contract_abi)
        return data["abi"]
