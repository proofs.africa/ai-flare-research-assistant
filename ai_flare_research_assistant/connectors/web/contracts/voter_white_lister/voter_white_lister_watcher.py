import asyncio
import logging
from typing import Optional, List

from komolibs.core.utils.async_utils import safe_ensure_future
from web3 import Web3
from web3.datastructures import AttributeDict

from ai_flare_research_assistant.chain.oracle.contracts.tso_contract_base import TsoContractBase
from ai_flare_research_assistant.chain.watcher.base_watcher import BaseWatcher
from ai_flare_research_assistant.chain.watcher.contract_event_logs import ContractEventLogger
from ai_flare_research_assistant.chain.watcher.websocket_watcher import WSNewBlocksWatcher

VOTER_REMOVED_EVENT_NAME = "VoterRemovedFromWhitelist"
VOTER_WHITE_LISTED_EVENT_NAME = "VoterWhitelisted"


class VoteWhiteListerWatcher(BaseWatcher):

    def __init__(self,
                 w3: Web3,
                 voter_white_lister: TsoContractBase,
                 blocks_watcher: WSNewBlocksWatcher,
                 new_blocks_queue: asyncio.Queue,
                 output: asyncio.Queue):
        super().__init__(w3)
        self._blocks_watcher: WSNewBlocksWatcher = blocks_watcher
        self._voter_white_lister = voter_white_lister
        self._vote_white_lister_contract = voter_white_lister.contract
        self._contract_event_logger = ContractEventLogger(w3, voter_white_lister.address, voter_white_lister.abi)
        self._poll_vote_white_lister_logs_task: asyncio.Task = None
        self._new_blocks_queue: asyncio.Queue = asyncio.Queue()

    async def start_network(self):
        if self._poll_vote_white_lister_logs_task is not None:
            await self.stop_network()
        self._poll_vote_white_lister_logs_task = safe_ensure_future(self.poll_vote_white_lister_logs_loop())

    async def stop_network(self):
        if self._poll_vote_white_lister_logs_task is not None:
            self._poll_vote_white_lister_logs_task.cancel()
            self._poll_vote_white_lister_logs_task = None

    async def poll_vote_white_lister_logs_loop(self):
        while True:
            try:
                new_blocks: List[AttributeDict] = await self._new_blocks_queue.get()

                voter_removed_entries = await self._contract_event_logger.get_new_entries_from_logs(
                    VOTER_REMOVED_EVENT_NAME,
                    new_blocks
                )

                voter_white_listed_entries = await self._contract_event_logger.get_new_entries_from_logs(
                    VOTER_WHITE_LISTED_EVENT_NAME,
                    new_blocks
                )
                for entry in voter_removed_entries:
                    await self._handle_event_data(entry)
                for entry in voter_white_listed_entries:
                    await self._handle_event_data(entry)

            except asyncio.CancelledError:
                raise
            except asyncio.TimeoutError:
                continue
            except Exception:
                self.logger().network("Unknown error trying to fetch new events from WETH contract.", exc_info=True,
                                      app_warning_msg="Unknown error trying to fetch new events from WETH contract. "
                                                      "Check wallet network connection")

    async def _handle_event_data(self, event_data: AttributeDict):
        event_type: str = event_data["event"]
        timestamp: float = float(await self._blocks_watcher.get_timestamp_for_block(event_data["blockHash"]))
        tx_hash: str = event_data["transactionHash"].hex()
        if event_type == VOTER_REMOVED_EVENT_NAME:
            self.voter_removed_from_white_list_event(timestamp, tx_hash, event_data)
        elif event_type == VOTER_WHITE_LISTED_EVENT_NAME:
            self.voter_white_listed_event(timestamp, tx_hash, event_data)
        else:
            self.logger().warning(f"Received log with unrecognized event type - '{event_type}'.")

    def voter_removed_from_white_list_event(self, timestamp: float, tx_hash: str, event_data: AttributeDict):
        event_args: AttributeDict = event_data["args"]

        voter_address: str = event_args["voter"]
        ftso_index: int = event_args["ftso_index"]

        # self.trigger_event(TsoEvent.VoterRemovedFromWhitelist,
        #                    VoterRemovedFromWhiteListEvent(
        #                        voter_address=voter_address,
        #                        ftso_index=ftso_index,
        #                        timestamp=timestamp))

    def voter_white_listed_event(self, timestamp: float, tx_hash: str, event_data: AttributeDict):
        event_args: AttributeDict = event_data["args"]

        voter_address: str = event_args["voter"]
        ftso_index: int = event_args["ftso_index"]

        # self.trigger_event(TsoEvent.VoterWhitelisted,
        #                    VoterWhiteListedEvent(
        #                        voter_address=voter_address,
        #                        ftso_index=ftso_index,
        #                        timestamp=timestamp))
