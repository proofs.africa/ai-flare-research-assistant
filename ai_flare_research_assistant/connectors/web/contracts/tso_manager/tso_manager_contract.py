import json
import os

from web3 import Web3

from ai_flare_research_assistant.connectors.web.contracts.tso_contract_base import TsoContractBase


class TSOManagerContract(TsoContractBase):

    def __init__(self,
                 w3: Web3,
                 address: str,
                 chain: str):
        super().__init__(w3, address, chain)

    @property
    def name(self):
        return "tso_manager"

    @property
    def abi(self):
        with open(
                os.path.join(os.path.dirname(__file__), f'tso_manager_contract_abi.{self.chain}.json')) as contract_abi:
            data: dict = json.load(contract_abi)
        return data["abi"]

    @property
    def current_price_epoch(self):
        return self.contract.functions.getCurrentPriceEpochData().call()[0]

    @property
    def current_price_epoch_data(self):
        return self.contract.functions.getCurrentPriceEpochData().call()

    @property
    def current_reward_epoch(self):
        return self.contract.functions.getCurrentRewardEpoch().call()

