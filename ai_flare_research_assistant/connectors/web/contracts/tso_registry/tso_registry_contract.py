import json
import os

from web3 import Web3

from ai_flare_research_assistant.connectors.web.contracts.tso_contract_base import TsoContractBase


class TSORegistryContract(TsoContractBase):

    def __init__(self,
                 w3: Web3,
                 address: str,
                 chain: str):
        super().__init__(w3, address, chain)

    @property
    def name(self):
        return "tso_registry"

    @property
    def abi(self):
        with open(
                os.path.join(os.path.dirname(__file__), f'tso_registry_contract_abi.{self.chain}.json')) as contract_abi:
            data: dict = json.load(contract_abi)
        return data["abi"]

    @property
    def tso_address_pairs(self):
        sisaf = self.contract.functions.getSupportedIndicesSymbolsAndFtsos().call()
        supported_symbol_indices_pairs = {record[1]: record[0] for record in zip(sisaf[1], sisaf[2])}

        return supported_symbol_indices_pairs

    @property
    def tso_index_pairs(self):
        sisaf = self.contract.functions.getSupportedIndicesSymbolsAndFtsos().call()
        supported_symbol_indices_pairs = {record[1]: record[0] for record in zip(sisaf[1], sisaf[0])}

        return supported_symbol_indices_pairs

    @property
    def all_current_prices(self):
        latest = self.contract.functions.getAllCurrentPrices().call()
        return latest

    def get_tso_price(self, asset):
        return self.contract.functions.getCurrentPriceWithDecimals(asset).call()[0]
