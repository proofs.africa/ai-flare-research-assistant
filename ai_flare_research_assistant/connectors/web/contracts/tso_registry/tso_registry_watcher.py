import asyncio
import logging
from typing import Optional, List

from web3 import Web3
from web3.datastructures import AttributeDict

from ai_flare_research_assistant import app_logger
from ai_flare_research_assistant.chain.oracle.contracts.tso_contract_base import TsoContractBase
from ai_flare_research_assistant.chain.watcher.base_watcher import BaseWatcher
from ai_flare_research_assistant.chain.watcher.contract_event_logs import ContractEventLogger
from ai_flare_research_assistant.chain.watcher.websocket_watcher import WSNewBlocksWatcher
from ai_flare_research_assistant.core.utils.async_utils import safe_ensure_future

GOVERNANCE_PROPOSED_EVENT_NAME = "GovernanceProposedEvent"
GOVERNANCE_UPDATED_EVENT_NAME = "GovernanceUpdatedEvent"


class TsoRegistryWatcher(BaseWatcher):

    def __init__(self,
                 w3: Web3,
                 tso_registry: TsoContractBase,
                 blocks_watcher: WSNewBlocksWatcher,
                 new_blocks_queue: asyncio.Queue,
                 output: asyncio.Queue):
        super().__init__(w3)
        self._blocks_watcher: WSNewBlocksWatcher = blocks_watcher
        self._tso_registry = tso_registry
        self._output = output
        self._tso_registry_contract = tso_registry.contract
        self._contract_event_logger = ContractEventLogger(w3, tso_registry.address, tso_registry.abi)
        self._poll_tso_registry_logs_task: asyncio.Task = None
        self._new_blocks_queue: asyncio.Queue = new_blocks_queue

    async def start_network(self):
        if self._poll_tso_registry_logs_task is not None:
            await self.stop_network()
        self._poll_tso_registry_logs_task = safe_ensure_future(self.poll_tso_registry_logs_loop())

    async def stop_network(self):
        if self._poll_tso_registry_logs_task is not None:
            self._poll_tso_registry_logs_task.cancel()
            self._poll_tso_registry_logs_task = None

    async def poll_tso_registry_logs_loop(self):
        while True:
            try:
                new_blocks: List[AttributeDict] = await self._new_blocks_queue.get()

                governance_proposed_entries = await self._contract_event_logger.get_new_entries_from_logs(
                    GOVERNANCE_PROPOSED_EVENT_NAME,
                    new_blocks
                )

                governance_updated_entries = await self._contract_event_logger.get_new_entries_from_logs(
                    GOVERNANCE_UPDATED_EVENT_NAME,
                    new_blocks
                )
                for entry in governance_proposed_entries:
                    await self._handle_event_data(entry)
                for entry in governance_updated_entries:
                    await self._handle_event_data(entry)

            except asyncio.CancelledError:
                raise
            except asyncio.TimeoutError:
                await asyncio.sleep(0.5)
                continue
            except Exception:
                app_logger.error("Unknown error trying to fetch new events from TSO registry contract.",
                                 exc_info=True,
                                 app_warning_msg="Unknown error trying to fetch new events from TSO registry "
                                                 "contract. Check wallet network connection.")

    async def _handle_event_data(self, event_data: AttributeDict):
        event_type: str = event_data["event"]
        timestamp: float = float(await self._blocks_watcher.get_timestamp_for_block(event_data["blockHash"]))
        tx_hash: str = event_data["transactionHash"].hex()
        if event_type == GOVERNANCE_PROPOSED_EVENT_NAME:
            self.governance_proposed_event(timestamp, tx_hash, event_data)
        elif event_type == GOVERNANCE_UPDATED_EVENT_NAME:
            self.governance_updated_event(timestamp, tx_hash, event_data)
        else:
            app_logger.warning(f"Received log with unrecognized event type - '{event_type}'.")

    def governance_proposed_event(self, timestamp: float, tx_hash: str, event_data: AttributeDict):
        event_args: AttributeDict = event_data["args"]

        proposed_governance: str = event_args["proposed_governance"]

    def governance_updated_event(self, timestamp: float, tx_hash: str, event_data: AttributeDict):
        event_args: AttributeDict = event_data["args"]

        old_governance: str = event_args["old_governance"]
        new_governance: str = event_args["new_governance"]
