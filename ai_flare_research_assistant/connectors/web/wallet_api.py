from typing import Dict

from web3 import Web3
from web3.middleware import geth_poa_middleware

from ai_flare_research_assistant.connectors.web.contracts.price_submitter.price_submitter_contract import \
    TSOPriceSubmitterContract
from ai_flare_research_assistant.connectors.web.contracts.tso_contract_base import TsoContractBase
from ai_flare_research_assistant.connectors.web.contracts.tso_manager.tso_manager_contract import TSOManagerContract
from ai_flare_research_assistant.connectors.web.contracts.tso_registry.tso_registry_contract import TSORegistryContract
from ai_flare_research_assistant.connectors.web.contracts.voter_white_lister.voter_white_lister_contract import \
    TSOVoterWhiteListerContract
from ai_flare_research_assistant.connectors.web.wallet_base import WalletBase
from ai_flare_research_assistant.core.utils.time_formatter import give_me_time_in_iso


class WalletApi(WalletBase):
    BACKEND_SELECTION_INTERVAL = 15.0
    WALLET_EVENT_DEDUP_WINDOW_SIZE = 1024

    def __init__(self,
                 jsonrpc_url: str,
                 price_submitter_address: str,
                 chain: str):
        super().__init__()

        self._w3: Web3 = Web3(Web3.HTTPProvider(jsonrpc_url))
        self._w3.middleware_onion.inject(geth_poa_middleware, layer=0)
        self._chain: str = chain

        self._price_submitter_address = price_submitter_address
        self._price_submitter_contract = TSOPriceSubmitterContract(w3=self._w3, address=price_submitter_address,
                                                                   chain=chain)
        voter_white_list_address = self._price_submitter_contract.contract.functions.getVoterWhitelister().call()
        tso_registry_address = self._price_submitter_contract.contract.functions.getFtsoRegistry().call()
        tso_manager_address = self._price_submitter_contract.contract.functions.getFtsoManager().call()
        # Initialize price provider contract data structures.
        self._tso_contracts = {
            "price_submitter": self._price_submitter_contract,
            "voter_white_lister": TSOVoterWhiteListerContract(w3=self._w3, address=voter_white_list_address,
                                                              chain=chain),
            "tso_registry": TSORegistryContract(w3=self._w3, address=tso_registry_address, chain=chain),
            "tso_manager": TSOManagerContract(w3=self._w3, address=tso_manager_address, chain=chain),
        }

    @property
    def chain(self) -> str:
        return self._chain

    @property
    def tso_contracts(self) -> Dict[str, TsoContractBase]:
        return self._tso_contracts.copy()

    @property
    def get_current_block_number(self):
        return self._w3.eth.get_block_number()

    @property
    def get_current_reward_epoch(self):
        current_reward_epoch = self._tso_contracts["tso_manager"].contract.functions.getCurrentRewardEpoch().call()
        return current_reward_epoch

    @property
    def get_current_price_epoch(self):
        tso_epoch = self._tso_contracts["tso_manager"].contract.functions.getCurrentPriceEpochData().call()
        return {
            "price_epoch": tso_epoch[0],
            "start_time": give_me_time_in_iso(tso_epoch[1]),
            "submit_time": give_me_time_in_iso(tso_epoch[2]),
            "reveal_time": give_me_time_in_iso(tso_epoch[3]),
        }

    def get_price_by_asset(self, asset: str):
        print(f"The ASSET is {asset}")
        price = self._tso_contracts["tso_registry"].contract.functions.getCurrentPrice(asset.upper()).call()
        return {
            'price': price[0] / 100000,
            'time': give_me_time_in_iso(price[1])
        }
