import os

OPENAI_KEY=os.getenv("OPENAI_API_KEY")
EMBEDDING_MODEL="text-embedding-ada-002"
GENAI_MODEL='gpt-3.5-turbo'

PINECONE_KEY = os.getenv("PINECONE_API_KEY")
PINECONE_ENV = "gcp-starter"
PINECONE_INDEX_NAME = "default"  # this will be created below