import operator
from typing import Annotated, Any, Dict, List, Optional, Sequence, TypedDict
import functools

from langchain_core.messages import BaseMessage, HumanMessage
from langchain_core.prompts import ChatPromptTemplate, MessagesPlaceholder
from langgraph.graph import StateGraph, END

from ai_flare_research_assistant.ap.agents.flare_blockchain_data_agent import FlareBlockchainDataAgent
from ai_flare_research_assistant.ap.agents.flare_general_information_agent import FlareGeneralInformationAgent
from ai_flare_research_assistant.ap.agents.flare_staking_and_validation_agent import FlareStakingAndValidationAgent
from ai_flare_research_assistant.ap.chains.agent_supervisor import supervisor_chain, members


# The agent state is the input to each node in the graph
class AgentState(TypedDict):
    # The annotation tells the graph that new messages will always
    # be added to the current states
    messages: Annotated[Sequence[BaseMessage], operator.add]
    # The 'next' field indicates where to route to next
    next: str


def agent_node(state, agent, name):
    result = agent.invoke(state)
    return {"messages": [HumanMessage(content=result["output"], name=name)]}


class Assistant:
    def __init__(self):
        pass

    def create(self):
        general_information_agent = FlareGeneralInformationAgent().create()
        general_information_node = functools.partial(agent_node, agent=general_information_agent,
                                                     name="GeneralInformation")
        #
        blockchain_data_agent = FlareBlockchainDataAgent().create()
        blockchain_data_node = functools.partial(agent_node, agent=blockchain_data_agent, name="BlockchainData")

        # staking_and_validation_agent = FlareStakingAndValidationAgent().create()
        # staking_and_validation_node = functools.partial(agent_node, agent=staking_and_validation_agent,
        #                                                 name="StakingAndValidation")

        workflow = StateGraph(AgentState)
        workflow.add_node("GeneralInformation", general_information_node)
        workflow.add_node("BlockchainData", blockchain_data_node)
        # workflow.add_node("StakingAndValidation", staking_and_validation_node)
        workflow.add_node("supervisor", supervisor_chain)

        for member in members:
            # We want our workers to ALWAYS "report back" to the supervisor when done
            workflow.add_edge(member, "supervisor")
        # The supervisor populates the "next" field in the graph state
        # which routes to a node or finishes
        conditional_map = {k: k for k in members}
        conditional_map["FINISH"] = END
        workflow.add_conditional_edges("supervisor", lambda x: x["next"], conditional_map)
        # Finally, add entrypoint
        workflow.set_entry_point("supervisor")

        graph = workflow.compile()

        # for s in graph.stream(
        #         {
        #             "messages": [
        #                 HumanMessage(content="What is Flare?")
        #             ]
        #         }
        # ):
        #     if "__end__" not in s:
        #         print(s)
        #         print("----")

        return graph
