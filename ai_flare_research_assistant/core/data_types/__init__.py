from enum import Enum


class ActivityType(Enum):
    MIGRATE = 1
    TRAIN = 2
    PREDICT = 3
    MONITOR = 4
