from typing import Optional

from langchain import hub
from langchain.agents import AgentExecutor, create_structured_chat_agent
from langchain_openai import ChatOpenAI

from ai_flare_research_assistant.ap.toolkits.flare.toolkit import FlareToolkit
from ai_flare_research_assistant.ap.tools.general_info_tool import GeneralInfoTool
from ai_flare_research_assistant.ap.tools.staking_and_validation_tool import StakingAndValidationTool
from ai_flare_research_assistant.ap.utilities.flare import FlareAPIWrapper
from ai_flare_research_assistant.connectors.web.wallet_api import WalletApi

URL = 'https://flare-api.flare.network/ext/C/rpc'
CHAIN = 'flare'
SUBMITTER = '0x1000000000000000000000000000000000000003'


class AssistantAgent:
    def __init__(self):
        # Get the prompt to use - you can modify this!
        self.prompt = hub.pull("hwchase17/structured-chat-agent")
        print(f"1111 {self.prompt.messages}")

        self.agent_executor: Optional[AgentExecutor] = None

    def create(self):
        tools: list = []
        wallet = WalletApi(jsonrpc_url=URL, chain=CHAIN, price_submitter_address=SUBMITTER)
        flare = FlareAPIWrapper(wallet_instance=wallet)
        toolkit = FlareToolkit.from_flare_api_wrapper(flare)
        tools: list = toolkit.get_tools()
        tools.append(GeneralInfoTool())
        tools.append(StakingAndValidationTool())

        llm = ChatOpenAI(model="gpt-3.5-turbo-0613", temperature=0)
        # self.agent = create_structured_chat_agent(llm, tools, self.prompt)
        # Construct the JSON agent
        agent = create_structured_chat_agent(llm, tools, self.prompt)
        # STRUCTURED_CHAT includes args_schema for each tool, helps tool args parsing errors.
        # self.agent = initialize_agent(
        #     tools,
        #     llm,
        #     agent=AgentType.STRUCTURED_CHAT_ZERO_SHOT_REACT_DESCRIPTION,
        #     verbose=True,
        # )
        print("Available tools:")
        for tool in tools:
            print("\t" + tool.name)

        self.agent_executor = AgentExecutor(agent=agent, tools=tools, verbose=True)
        self.agent_executor.handle_parsing_errors = True

    def run(self, query: str):
        # return self.agent.run(query)
        return self.agent_executor.invoke({'input': query})['output']
