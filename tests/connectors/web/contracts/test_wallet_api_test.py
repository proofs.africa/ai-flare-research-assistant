import logging
import os
import sys
from os.path import realpath, join

from ai_flare_research_assistant.connectors.web.wallet_api import WalletApi
from ai_flare_research_assistant.logger.struct_logger import METRICS_LOG_LEVEL

sys.path.insert(0, realpath(join(__file__, "../../../../")))

import asyncio
from typing import (
    Optional
)
import time
import unittest
from web3 import Web3

logging.basicConfig(level=METRICS_LOG_LEVEL)

URL = 'https://flare-api.flare.network/ext/C/rpc'
CHAIN = 'flare'
SUBMITTER = '0x1000000000000000000000000000000000000003'


class WalletApiUnitTest(unittest.TestCase):
    wallet: Optional[WalletApi] = None
    w3: Web3 = None

    @classmethod
    def setUpClass(cls):
        cls.wallet = WalletApi(jsonrpc_url=URL, chain=CHAIN, price_submitter_address=SUBMITTER)

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_main(self):
        # Check the initial conditions. There should be a certain number of initial tokens before the test can be
        # carried out.
        print(self.wallet.get_current_block_number)
        print(self.wallet.get_current_reward_epoch)
        print(self.wallet.get_current_price_epoch)
        print(self.wallet.get_price_by_asset('XRP'))
