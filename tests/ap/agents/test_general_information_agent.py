import logging
import sys
from os.path import realpath, join

from langchain.agents import AgentExecutor

from ai_flare_research_assistant.ap.agents.flare_general_information_agent import FlareGeneralInformationAgent
from ai_flare_research_assistant.ap.agents.flare_staking_and_validation_agent import FlareStakingAndValidationAgent
from ai_flare_research_assistant.logger.struct_logger import METRICS_LOG_LEVEL

sys.path.insert(0, realpath(join(__file__, "../../../../")))

from typing import (
    Optional
)
import unittest

logging.basicConfig(level=METRICS_LOG_LEVEL)


class FlareGeneralInformationAgentUnitTest(unittest.TestCase):
    agent: Optional[AgentExecutor] = None

    @classmethod
    def setUpClass(cls):
        cls.agent = FlareGeneralInformationAgent().create()

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_main(self):
        print(self.agent.invoke({"input": "What is Flare?"}))
