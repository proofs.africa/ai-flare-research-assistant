import logging
import sys
from os.path import realpath, join

from langchain.agents import AgentExecutor

from ai_flare_research_assistant.ap.agents.flare_staking_and_validation_agent import FlareStakingAndValidationAgent
from ai_flare_research_assistant.logger.struct_logger import METRICS_LOG_LEVEL

sys.path.insert(0, realpath(join(__file__, "../../../../")))

from typing import (
    Optional
)
import unittest

logging.basicConfig(level=METRICS_LOG_LEVEL)


class FlareStakingAndValidationAgentUnitTest(unittest.TestCase):
    agent: Optional[AgentExecutor] = None

    @classmethod
    def setUpClass(cls):
        cls.agent = FlareStakingAndValidationAgent().create()

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_main(self):
        # Check the initial conditions. There should be a certain number of initial tokens before the test can be
        # carried out.
        doozzie = (f"Who are the top 10 validators by amount staked? Return in a table with staked amounts, self "
                   f"bonded amounts and the line total of the two. Remove the index column and "
                   f"add a rank column starting at 1.")


        doozzie = "Who is the most staked validator and what amount is staked?"
        print(doozzie)
        # print(self.agent.invoke({"input": "Who are the top 10 validators by amount staked?"}))
        print(self.agent.invoke({"input": doozzie}))
