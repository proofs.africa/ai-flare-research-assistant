import logging
import sys
from os.path import realpath, join

from langchain.agents import AgentExecutor
from langchain_core.messages import HumanMessage
from langgraph.pregel import Pregel

from ai_flare_research_assistant.ap.agents.flare_general_information_agent import FlareGeneralInformationAgent
from ai_flare_research_assistant.ap.agents.flare_staking_and_validation_agent import FlareStakingAndValidationAgent
from ai_flare_research_assistant.assistant import Assistant
from ai_flare_research_assistant.logger.struct_logger import METRICS_LOG_LEVEL

sys.path.insert(0, realpath(join(__file__, "../../../../")))

from typing import (
    Optional
)
import unittest

logging.basicConfig(level=METRICS_LOG_LEVEL)


class AssistantUnitTest(unittest.TestCase):
    graph: Optional[Pregel] = None

    @classmethod
    def setUpClass(cls):
        cls.graph = Assistant().create()

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_main(self):

        # q = "What is Flare?"
        q = "How is the price of XRP?"
        for s in self.graph.stream(
                {
                    "messages": [
                        HumanMessage(content=q)
                    ]
                }
        ):
            if "__end__" not in s:
                print(s)
                print("----")
        # print(self.agent.invoke({"input": "What is Flare?"}))
