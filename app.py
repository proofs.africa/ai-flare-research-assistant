from ai_flare_research_assistant.assistant_agent import AssistantAgent

agent = AssistantAgent()
agent.create()

# Import required libraries

import streamlit as st

# Title
st.title('🤖 Flare Assistant  ')

# Welcoming message
st.write("Hello, 👋 I am your Flare AI assistance. I am here to help you with information about the Flare ecosystem. ")

# Explanation sidebar
with st.sidebar:
    st.write('*Important Disclaimer.*')
    st.caption('''**At this stage, this projected is only intended to be experimental. DO NOT USE ANY INFORMATION 
    FROM THIS APP AS A BASIS FOR FINANCIAL DECISIONS. Furthermore, although we are comfortable on the AI's competence 
    in providing accurate generic information about the Flare ecosystem, we suggest that the reader does use the 
    references provided in order to validate gained knowledge. We will, from time to time, review the performance of 
    the AI and update the reader on confidence levels.
    **
    ''')

    st.divider()

    st.caption("<p style ='text-align:center'> made with ❤️ by African Proofs</p>", unsafe_allow_html=True)


if "messages" not in st.session_state:
    st.session_state.messages = []

for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

if prompt := st.chat_input("What is up?"):
    st.session_state.messages.append({"role": "user", "content": prompt})
    with st.chat_message("user"):
        st.markdown(prompt)
    with st.chat_message("assistant"):
        message_placeholder = st.empty()
        full_response = agent.run(prompt)

        message_placeholder.markdown(full_response)
    st.session_state.messages.append({"role": "assistant", "content": full_response})